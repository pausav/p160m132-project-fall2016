# This directory is synchronized between bdmm-ansible-vagrant-ubuntu32/synced_dir in host OS and ~/synced_dir in guest OS

- synced by vagrant to /home/vagrant/synced_dir (~/synced_dir)
- contents of this directory is not tracked by `git`, therefore it is possible to put other `git` repositories inside for personal use.
